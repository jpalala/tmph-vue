# vue-tasks

> A Vue.js Todo project for teaching basic vue

Clone the repo: 

`git clone https://gitlab.com/dev-templates/laravel-backend`

## Permission denied? "Project does not exist"?

You can find the same codes (laravel + vue) on my github.com/jpalala/dev-templates

##Cloning from Gitlab (because you want to contribute, I guess?):

I noticed some issues from gitlab where cloning via git+ssh gives some errors.

You may need to supply your ssh key. To do so,  create a gitlab account, and upload your ssh public key. 


You may also need to add the following to your `~/.ssh/config` file (usually your pub key is id_rsa.pub - don't
forget to change this below).

```
Host gitlab.com
    PubkeyAcceptedKeyTypes +ssh-rsa
    HostName gitlab.com
    IdentityFile ~/.ssh/*your_public_key*
    User git
```

This is to solve the "[The project you were looking for could not be found.](https://gitlab.com/gitlab-com/support-forum/issues/638)" issue.

## Using vue-cli

You can start from scratch and create a vue project by doing the following:

`npm install -g @vue/cli` and generate a fresh vue project  by running `vue create hello-world`


Note: You can also check [vue-cli installation guide](https://cli.vuejs.org/guide/installation.html) to install vue-cli and follow the instructions located at [cli.vuejs.org/guide/creating-a-project.html](https://cli.vuejs.org/guide/creating-a-project.html)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
